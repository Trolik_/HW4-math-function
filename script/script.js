"use strict";
// debugger
let inputFirstNumber = prompt("Please, enter your first number:");
let inputSecondNumber = prompt("Please, enter your second number:");
let inputOperator = prompt("Please, enter your math operator ('+', '-', '*', '/'):");

function validation () {
    if (isNaN(inputFirstNumber) || isNaN(inputSecondNumber) || inputFirstNumber === "" || inputSecondNumber === "") {
        alert("You have not entered a number or entered incorrect one!!")

        inputFirstNumber = prompt("Please, enter correct first number:");
        inputSecondNumber = prompt("Please, enter correct second number:");
    }

    let mathOperators = "+-*/";

    while (!mathOperators.includes(inputOperator)) {
        inputOperator = prompt("Please, enter correct math operator ('+', '-', '*', '/'):");
    }
}

function mathOperation (first, second, operator) {
    if (operator === "+") {
        return console.log(`Sum result: ${first} + ${second} = ${first + second}`);

    } else if (operator === "-") {
        return console.log(`Subtraction result: ${first} - ${second} = ${first - second}`);

    } else if (operator === "*") {
        return console.log(`Multiplication result: ${first} * ${second} = ${first * second}`);

    } else if (operator === "/") {
        if ((first / second) === Infinity) {
            alert("You have entered a zero for second number!\n" +
                "Note: Don't use zero for second number. You can not divide by 0!!!")

            inputFirstNumber = prompt("Please, enter your first number:");
            inputSecondNumber = prompt("Please, enter your second number:");
            inputOperator = prompt("Please, enter your math operator('+', '-', '*', '/'):");

            validation();
            mathOperation(inputFirstNumber, inputSecondNumber, inputOperator);

        } else return console.log(`Division result: ${first} / ${second} = ${first / second}`);
    }
}

validation();
mathOperation(inputFirstNumber, inputSecondNumber, inputOperator);